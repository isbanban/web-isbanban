function initialize(latitude, longitude) {
    var mapStyles = [{"featureType":"all","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels.text","stylers":[{"visibility":"simplified"}]}];

    var center = new google.maps.LatLng(latitude, longitude);

    var map = new google.maps.Map(document.getElementById('mapLocation'), {
        zoom: 17,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        mapTypeControl: false,
        streetViewControl: false,
        scaleControl: false,
        scrollwheel: false,
        draggable: true,
        styles: mapStyles,
    });

    google.maps.event.addListener(map, 'dragend', function() {
        locationDraw(latitude, longitude, map, new google.maps.Geocoder);
    });

    google.maps.event.addListener(map, 'zoom_changed', function() {
        locationDraw(latitude, longitude, map, new google.maps.Geocoder);
    });

    initSearchbox(map);
    addYourLocationButton(map);
}

// Initialize Searchbox
function initSearchbox(map) {
    var input            = document.getElementById('location-search');
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(input);

    var searchBox = new google.maps.places.Autocomplete(document.getElementById('location-search'), {
        types: ['geocode'],
        componentRestrictions: {
            country: 'id'
        }
    });

    searchBox.addListener('place_changed', function() {
        var place = searchBox.getPlace();

        if (place.geometry.viewport) {

            map.fitBounds(place.geometry.viewport);
            map.setZoom(17);

        } else {

            map.setCenter(place.geometry.location);
            map.setZoom(17);

        }
    });
}

// Initialize Fire Location
function locationDraw(latitude, longitude, map, geocoder) {
    var newLocation = map.getCenter();
    var newLatitude = newLocation.lat();
    var newLongitude= newLocation.lng();
    var LatLong = {lat: parseFloat(newLocation.lat()), lng: parseFloat(newLocation.lng())};

    $("[data-toggle=locationMarkerLatitude").val(newLatitude);
    $("[data-toggle=locationMarkerLongitude").val(newLongitude);
    $("[data-toggle=locationName]").text();

    locationRead(LatLong, geocoder);
}

// Initialize Watcher Location
function locationRead(LatLong, geocoder) {
    geocoder.geocode({'location': LatLong}, function(results, status) {
        if (status === 'OK') {
            if (results[3]) {

                console.log(results);

                $("[data-toggle=locationName]").text(results[3].formatted_address);

            } else {

                $.oc.flashMsg({ text: 'No results found', class: 'info' })
            }

        } else {

            $.oc.flashMsg({ text: status, class: 'info' })

        }
    });
}

// Initialize Current Location Button
function addYourLocationButton(map)  {
    var controlDiv = document.createElement('div');
    var firstChild = document.createElement('button');
    firstChild.setAttribute('type', 'button');
    firstChild.style.backgroundColor = '#fff';
    firstChild.style.border = 'none';
    firstChild.style.outline = 'none';
    firstChild.style.width = '28px';
    firstChild.style.height = '28px';
    firstChild.style.borderRadius = '2px';
    firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
    firstChild.style.cursor = 'pointer';
    firstChild.style.marginRight = '10px';
    firstChild.style.padding = '0px';
    firstChild.title = 'Your Location';
    controlDiv.appendChild(firstChild);

    var secondChild = document.createElement('div');
    secondChild.style.margin = '5px';
    secondChild.style.width = '18px';
    secondChild.style.height = '18px';
    secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-1x.png)';
    secondChild.style.backgroundSize = '180px 18px';
    secondChild.style.backgroundPosition = '0px 0px';
    secondChild.style.backgroundRepeat = 'no-repeat';
    secondChild.id = 'you_location_img';
    firstChild.appendChild(secondChild);

    google.maps.event.addListener(map, 'dragend', function() {
        $('#you_location_img').css('background-position', '0px 0px');
    });

    firstChild.addEventListener('click', function() {
        var imgX = '0';
        var animationInterval = setInterval(function(){
            if(imgX == '-18') imgX = '0';
            else imgX = '-18';
            $('#you_location_img').css('background-position', imgX+'px 0px');
        }, 500);

        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(latlng);
                locationDraw(position.coords.latitude, position.coords.longitude, map, new google.maps.Geocoder);
                clearInterval(animationInterval);
                $('#you_location_img').css('background-position', '-144px 0px');
            });

        } else {

            clearInterval(animationInterval);
            $('#you_location_img').css('background-position', '0px 0px');
            $.oc.flashMsg({ text: 'We need to access your location', class: 'info' })
        }
    });

    controlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(controlDiv);
}

$(function(){
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            $("[data-toggle=currentLatitude], [data-toggle=locationMarkerLatitude]").val(position.coords.latitude);
            $("[data-toggle=currentLongitude], [data-toggle=locationMarkerLongitude]").val(position.coords.longitude);

            initialize(position.coords.latitude, position.coords.longitude);
        });
    }
});
