$(document).ready(function() {

    // Attach fastclick
    FastClick.attach(document.body);

    // Activate tooltip bootstrap
    $('[data-toggle=tooltip]').tooltip({
        container: 'body',
    });

    // Init Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Init Zoomjs
    $(".text-content").find("img").attr('data-action', 'zoom');

    // Dropdown
    $('.dropdown-main-menu').on('show.bs.dropdown', function () {
        $("body").addClass("is-dropdown");
    }).on('hidden.bs.dropdown', function(){
        $("body").removeClass("is-dropdown");
    });

    $(".dropdown-menu-primary").click(function(e){
        e.stopPropagation();
    });

    // Navbar Watcher
    $(document).on('scroll', function() {
        if (window.scrollY > 160) {
            $(".navbar").addClass("navbar-shadow");
        }

        if (window.scrollY < 160) {
            $(".navbar").removeClass("navbar-shadow");
        }
    });
});
