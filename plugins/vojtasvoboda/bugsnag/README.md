# Bugsnag plugin for OctoberCMS

This plugin extend [Error Logger](http://octobercms.com/plugin/vojtasvoboda-errorlogger) and adds support for [Bugsnag](https://bugsnag.com/) error logger.

Before install be sure you have Error Logger installed.

## Installation

1. Install this plugin.
2. Register to [BugSnag](https://bugsnag.com/) and copy your API KEY.
3. Set your API KEY at Backend > Settings > System > Error logger > Bugsnag.

## Using PHP 5.6 or PHP 7.0?

Bugsnag plugin is made to be compatible with OctoberCMS, which means to be compatible with PHP 5.5.9. But if you are using PHP 5.6 or higher, you can use special [branch php56](https://github.com/vojtasvoboda/oc-bugsnag-plugin/tree/php56), where you can find same plugin, but with latest Bugsnag libraries used.

## Contributing

Please send Pull Request to master branch.

## License

Bugsnag plugin is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT) same as OctoberCMS platform.
