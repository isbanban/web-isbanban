<?php namespace Web\Location\Components;

use Bridge\Location\Models\Province;
use Bridge\Location\Models\Regency;
use Bridge\Location\Models\District;
use Bridge\Location\Models\Village;

use Cms\Classes\ComponentBase;

class Location extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Location Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getProvince()
    {
        return Province::orderBy('name', 'asc')->get();
    }

    public function getRegency($provinceId)
    {
        return Regency::whereProvinceId($provinceId)->orderBy('name', 'asc')->get();
    }

    public function getDistrict($regencyId)
    {
        return District::whereRegencyId($regencyId)->orderBy('name', 'asc')->get();
    }

    public function getVillage($districtId)
    {
        return Village::whereDistrictId($districtId)->orderBy('name', 'asc')->get();
    }

    public function onGetRegency()
    {
        $regency = Regency::whereProvinceId(post('province_id'))->orderBy('name', 'asc')->get();
        $this->page['regencies'] = $regency;
    }

    public function onGetDistrict()
    {
        $district = District::whereRegencyId(post('regency_id'))->orderBy('name', 'asc')->get();
        $this->page['districts'] = $district;
    }

    public function onGetVillage()
    {
        $village = Village::whereDistrictId(post('district_id'))->orderBy('name', 'asc')->get();
        $this->page['villages'] = $village;
    }
}
