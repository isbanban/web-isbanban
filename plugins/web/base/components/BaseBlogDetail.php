<?php namespace Web\Base\Components;

use DB;
use Redirect;

use Isbanban\Blog\Models\Blog;

use Cms\Classes\ComponentBase;

class BaseBlogDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseBlogDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $blog = $this->getCurrent();

        if(!$blog) {
            return Redirect::to('404');
        }

        $this->page['blog']       = $blog;
        $this->page['randomBlog'] = $this->getRandomBlog();
    }

    public function getCurrent()
    {
        return Blog::whereSlug($this->property('parameter'))->first();
    }

    public function getRandomBlog()
    {
        return Blog::take(6)->orderBy(DB::raw('RAND()'))->get();
    }
}
