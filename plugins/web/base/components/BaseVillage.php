<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseVillage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseVillage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
