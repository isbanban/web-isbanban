<?php namespace Web\Base\Components;

use Flash;
use Redirect;

use Isbanban\Core\Models\Program;

use Cms\Classes\ComponentBase;

class BaseProgramDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseProgramDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $program = $this->getCurrent();
        if(!$program) {
            return Redirect::to('404');
        }

        $this->page['program'] = $program;
    }

    public function getCurrent()
    {
        return Program::whereSlug($this->property('parameter'))->first();
    }
}
