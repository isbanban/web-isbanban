<?php namespace Web\Base\Components;

use Redirect;

use Isbanban\Core\Models\Region;

use Isbanban\Village\Models\Village;

use Cms\Classes\ComponentBase;

class BaseVillageChapter extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseVillageChapter Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $village = $this->getCurrent();
        if(!$village) {
            return Redirect::to('/404');
        }

        $this->page['villages'] = $village;
    }

    public function getCurrent()
    {
        $region = Region::whereSlug($this->property('parameter'))->first();
        return Village::whereRegionId($region->id)->get();
    }
}
