<?php namespace Web\Base\Components;

use Redirect;

use Isbanban\Volunteer\Models\VolunteerDepartment;

use Cms\Classes\ComponentBase;

class BaseVolunteerResourceDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseVolunteerResourceDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $resource = $this->getCurent();
        if(!$resource) {
            return Redirect::to('/404');
        }

        $this->page['resource'] = $resource;
    }

    public function getCurent()
    {
        return VolunteerDepartment::whereSlug($this->property('parameter'))->first();
    }
}
