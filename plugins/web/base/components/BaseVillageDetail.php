<?php namespace Web\Base\Components;

use Redirect;

use Isbanban\Volunteer\Models\Volunteer;

use Isbanban\Village\Models\Village;

use Cms\Classes\ComponentBase;

class BaseVillageDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseVillageDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $village = $this->getCurrent();
        if(!$village) {
            return Redirect::to('/404');
        }

        $this->page['village']         = $village;
        $this->page['volunteers']      = Volunteer::orderBy('name', 'asc')->whereChapterCode($village->chapter->code)->paginate(12);
    }

    public function getCurrent()
    {
        return Village::whereSlug($this->property('parameter'))->first();
    }
}
