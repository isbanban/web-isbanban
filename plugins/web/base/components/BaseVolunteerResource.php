<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseVolunteerResource extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseVolunteerResource Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
