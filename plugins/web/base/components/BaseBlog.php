<?php namespace Web\Base\Components;

use Isbanban\Blog\Models\Blog as BlogModel;

use Cms\Classes\ComponentBase;

class BaseBlog extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseBlog Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['blogs'] = $this->getBlog();
    }

    public function getBlog()
    {
        return BlogModel::orderBy('created_at', 'desc')->paginate(6);
    }
}
